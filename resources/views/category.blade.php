@extends('layouts.app')

@section('content')
  <h2 class="text-upper text-center section-title mt-5 mb-5">@php single_term_title() @endphp</h2>
  @while (have_posts()) 
    <div class="row news">
      <div class="col-md-8 offset-md-2">
      @php the_post() @endphp
      @include('partials.content-'.get_post_type())
      </div>
    </div>
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
