{{--
  Template Name: Meubelbewaring
--}}

@extends('layouts.app-dac')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-meubelbewaring')
  @endwhile
@endsection
