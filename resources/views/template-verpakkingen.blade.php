{{--
  Template Name: Verpakkingen
--}}

@extends('layouts.app-dac')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-verpakkingen')
  @endwhile
@endsection
