{{--
  Template Name: Verhuizingen
--}}

@extends('layouts.app-dac')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-verhuizingen')
  @endwhile
@endsection
