{{--
  Template Name: Contact
--}}

@extends('layouts.app-dac')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-contact')
  @endwhile
@endsection
