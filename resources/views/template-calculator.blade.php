{{--
  Template Name: Calculator
--}}

@extends('layouts.app-dac')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @if (get_field('page_layout') == 'pakketcustom')
    @include('partials.content-calculator')
  @elseif (get_field('page_layout') == 'pakketten')
    @include('partials.content-calculator-pakketen')
  @elseif (get_field('page_layout') == 'custom')
    @include('partials.content-calculator-custom')
  @endif

  @endwhile
@endsection
