{{--
  Template Name: Referenties
--}}

@extends('layouts.app-dac')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-referenties')
  @endwhile
@endsection
