<div class="referenties container mt-5 mb-5">
  <div class="m-auto">
    <h1 class="section-title text-center mb-3">VERHUIZEN MET VERHUIZINGEN DAC? <br />ONZE REFERENTIES VERTELLEN MEER</h1>
    <p class="text-center mb-5">Bent u benieuwd wie we bij Verhuizingen DAC helpen met verhuizen?<br /> Ontdek hieronder enkele referenties van tevreden klanten.</p>
  </div>
  <div class="d-md-flex row justify-content-left h-boxes">
      <?php
        // check if the repeater field has rows of data
        if( have_rows('referenties') ):

          // loop through the rows of data
            while ( have_rows('referenties') ) : the_row(); ?>
              <div class="col-md-4 text-center d-flex mb-5">
                <div class="box p-3">
                  <a href="<?php the_sub_field('link'); ?>">
                    <div class="img-wrap">
                      <img src="<?php the_sub_field('image'); ?>" class="img-fluid" />
                    </div>
                    <h3 class="box-title mt-3">
                      <?php  // display a sub field value
                        the_sub_field('title'); ?>
                    </h3>
                    <p class="mt35"><?php  // display a sub field value
                      the_sub_field('text'); ?>
                    </p> 
                  </a> 
                </div>
              </div>

             <?php endwhile;

        endif;

      ?>
    </div>
</div>
