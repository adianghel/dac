<div id="footerCarousel" class="slider slickslider justify-content-center d-flex">
  <?php
  // check if the repeater field has rows of data
  if( have_rows('footer_logos', 'option') ):
    // loop through the rows of data
      while ( have_rows('footer_logos', 'option') ) : the_row();
          // display a sub field value
          echo '<div><img src="'.get_sub_field('image', 'option').'" class="img-fluid" /></div>';
      endwhile;
  endif;
  ?>
</div>

<footer class="content-info" id="footer">
  <div class="container">
    <div class="col pl-md-4 pr-md-4">
      @php dynamic_sidebar('sidebar-footer') @endphp
      <div class="row">
        <div class="col-md-3">
          <ul id="footer-menu">
            <li>
              <strong class="mt-5 mb-3">Verhuizen</strong>
              <ul>
                <li><a href="<?php echo site_url(); ?>/verhuizingen/">Waarom DAC</a></li>
                <li><a href="<?php echo site_url(); ?>/verhuizingen/#belgie">Binnenland</a></li>
                <li><a href="<?php echo site_url(); ?>/verhuizingen/#buitenland">Buitenland</a></li>
                <li><a href="<?php echo site_url(); ?>/verhuizingen/#buitenland">Overzeese verhuizing</a></li>
                <li><a href="<?php echo site_url(); ?>/liftservice/">Verhuisdiensten</a></li>
                <li><a href="<?php echo site_url(); ?>/liftservice/#hijswerken">Hijswerken</a></li>
                <li><a href="<?php echo site_url(); ?>/verhuizingen/#verhuistips">Verhuistips</a></li>
                <li><a href="<?php echo site_url(); ?>/referenties/">Bedrijfsverhuis</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="col-md-3">
          <ul>
            <li>
              <strong class="mt-5 mb-3"><a href="<?php echo site_url(); ?>/meubelbewaring/">Meubelbewaring</a></strong>
              <ul>
                 <li><a href="<?php echo site_url(); ?>/bedrijfsverhuis/"><strong class="mt-5 mb-3">Bedrijfsverhuis</strong></a></li>
              </ul>
              <ul>
                 <li><a href="<?php echo site_url(); ?>/liftservice/"><strong class="mt-5 mb-3">Verhuislift</strong></a></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="col-md-3">
          <ul>
            <li>
              <strong class="mt-5 mb-3">Onze Lokale punten</strong>
              <ul>
                <li>Tongeren: 012 23 39 81</li>
                <li>Lanaken: 089 41 72 09</li>
                <li>Brussel: 02 502 87 83</li>
                <li>Antwerpen: 03 225 30 01</li>
                <li>Leuven: 016 20 50 72</li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="col-md-3 mt-5">
          <img src="{{ get_stylesheet_directory_uri() }}/assets/images/logo-footer.png"  class="img-fluid" alt="DAC"/>
        </div>
      </div>
    </div>
    <div class="footer-contact d-lg-flex">
      <span class="address">Kempische Steenweg 471A, 3500 Hasselt </span>
      <span class="phone">@php the_field('contact_phone', 'option'); @endphp </span>
      <span class="email">@php the_field('contact_email', 'option'); @endphp</span>
      <a href="https://www.facebook.com/dacverhuizingen/" class="facebook ml-auto" target="_blank"></a>
    </div>
    <div class="sub-footer mt-3 mb-3">
      <span>DACNV <?php echo date('Y'); ?> - BTW: BE 0471.765.438</span> - <span><a href="<?php echo site_url(); ?>/wp-content/uploads/2019/12/DACNV_Algemene_voorwaarden.pdf" target="_blank">Algemene voorwaarden</a></span> - <span><a href="<?php echo site_url(); ?>/wp-content/uploads/2019/12/DACNV_Privacy.pdf" target="_blank">Privacy</a></span>
    </div>
  </div>
</footer>
<div class="sticky-footer d-flex">
  <div class="yellow-bg">
    <img src="{{ get_stylesheet_directory_uri() }}/assets/images/icon-Erkend-Verhuizer.svg" width="70" class="d-inline-block" />
  </div>
  <div class="">
    <a href="https://www.facebook.com/dacverhuizingen/" class="facebook" target="_blank"></a>
  </div>
</div>