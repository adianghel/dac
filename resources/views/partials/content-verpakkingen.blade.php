<div class="intro pb-3 pb-md-5 container">
  <div class="row mb-md-5">
    <div class="col-md-6 mt-md-5 mb-md-5">
      <h1 class="section-title mb-3">Verhuisdozen en meer verhuismateriaal</h1>
      <p>U wil verhuizen en zoekt stevige verhuisdozen? Bij Verhuizingen DAC kan u terecht voor professioneel verpakkingsmateriaal. Ons gamma is de perfecte beschutting tegen schokken, vocht en vuil. Zo geraken uw meubels en andere belangrijke spullen schadevrij op uw bestemming.</p>
      <p>Verhuizingen DAC voorziet u van volgend verhuismateriaal:</p>
      <ul class="check-list">
        <li>Verhuisdozen (nieuw of tweedehands)</li>
        <li>Beschermende verhuisdekens</li>
        <li>Garderobedozen</li>
        <li>Ander beschermingsmateriaal</li>
      </ul>
      <a href="<?php echo site_url(); ?>/contact/" class="btn btn-red calculator box mt-4 mb-3 w-80 text-center">IK WIL VERHUISDOZEN BESTELLEN</a>
      <p class="mb-3 extra-text blue-color"><a href="<?php echo site_url(); ?>/verhuizen/#verhuistips">Ontdek ook onze verhuistips voor een veilig en schadevrij transport</a></p>  
    </div>
  </div>
</div>
<div class="bg-white">
  <div class="container">
    <div class="row pb-5">
      <div class="mt-5 col-md-6">
        <h2 class="section-subtitle mb-3">Verhuisdekens en ander beschuttend materiaal</h2>
        <p>Naast verhuisdozen vindt u bij ons ander beschuttend materiaal zoals verhuisdekens. Zo voorkomt u iedere vorm van schade en blijven uw spullen optimaal beschermd tijdens het transport. U kan ervoor kiezen verhuisdekens te kopen of huren. Er zijn steeds verschillende formaten beschikbaar. Voor voorwerpen die u niet makkelijk in verhuisdozen krijgt omwille van de grootte of vorm, gebruikt u best beschermende dekens. Lees ook onze <a href="<?php echo site_url(); ?>/verhuizen/#verhuistips">verhuistips</a> zodat u zich optimaal kan voorbereiden. </p>
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow mt-3 mb-3 text-center text-upper">Graag uw voorraad verhuisdozen bestellen?</a>
        <p class="mb-3 extra-text blue-color"><a href="<?php echo site_url(); ?>/contact/">Onze experts helpen u graag met uw bestelling</a></p>
      </div>
      <div class="col-md-6 mt-3 mt-m5">
        <img src="{{ get_stylesheet_directory_uri() }}/assets/images/liftservice/lift-piano.png"  class="img-fluid" width="440" />
        <div class="mt-5">
          <p>Ook voor tijdelijke stockage van uw spullen bieden we diverse formules aan voor <a href="<?php echo site_url(); ?>/meubelbewaring/">meubelbewaring</a>. Bent u, naast verhuisdozen, ook op zoek naar een <a href="<?php echo site_url(); ?>/liftservice/">verhuislift</a>, bestel- of aanhangwagen? <a href="<?php echo site_url(); ?>/contact/">Maak een afspraak met onze verhuisexperts</a> of <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/">bereken  uw verhuisprijs</a>.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="trans-intern">
  <div class="container">
    <div class="row pt-5 pb-5">
      <div class="col-md-6 mb-5">
        <h2 class="section-subtitle mb-3">Stevige verhuisdozen met lange levensduur</h2>
        <h3 class="section-subtitle-h3">Voor kleine en grote spullen</h3>
        <p>Onze verhuisdozen zijn geschikt voor zowel kleine als grote spullen. Ze zijn super stevig en beschikbaar in verschillende formaten. Het volstaat om de boven- en onderkant dicht te tapen. Bovendien heeft u de keuze uit nieuwe verhuisdozen of budgetvriendelijke tweedehands modellen. Voor kledij raden we onze handige garderobedozen aan, voor zware spullen zoals boeken, servies of bestek gebruikt u het best een plastic doos. </p>
        <p>Zorg bij het vullen van de verhuisdozen ervoor dat u eventuele gaten opvult met beschermende noppen- of schuimfolie. Zo schuiven uw spullen niet tijdens het transport en vangt de folie eventuele schokken tijdens de rit op. </p>
      </div>
      <div class="col-md-6">
        <h3 class="section-subtitle-h3">Gebruiksvriendelijk en stevig</h3>
        <p>Onze verhuisdozen zijn beschikbaar in verschillende handige formaten, zijn steeds groot genoeg om uw spullen in op te bergen en klein genoeg om te dragen zonder uw rug te overbelasten. Dankzij de gebruiksvriendelijke handgrepen kan u de verhuisdozen makkelijk oppakken en verplaatsen. Omdat ze zo stevig zijn, gebruikt u ze probleemloos meerdere keren. </p>
        <div class="text-center">
          <img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/DACnv_verhuismateriaal.png"  class="img-fluid" width="400" />
        </div>
      </div>
    </div>
  </div>
</div>