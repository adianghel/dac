<div class="homeh-wrap">
  <div class="shape">
    <div class="container">
      <div class="col-lg-8 home-header d-flex align-items-start flex-column">
        <h1 class="heading text-upper">Verhuizingen DAC: uw verhuisfirma in Limburg met meer dan 20 jaar ervaring</h1>
        <span class="font-cav d-block">Bereken de kosten van uw verhuizing!</span>
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-red mt-3 calculator text-upper">Naar de verhuiscalculator</a>
        <img src="{{ get_stylesheet_directory_uri() }}/assets/images/icon-Erkend-Verhuizer.svg" width="150" class="d-none d-lg-inline-block mt-3" />
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow mobileOfferte contact-link mt-3 mr-0 d-md-none">Gratis offerte</a>
      </div>
    </div><!-- /.container -->
  </div><!-- /.shape -->
  <div class="container">
    <div class="d-md-flex justify-content-center h-boxes">
      <?php

        // check if the repeater field has rows of data
        if( have_rows('header_boxes') ):

          // loop through the rows of data
            while ( have_rows('header_boxes') ) : the_row(); ?>
              <div class="col-md-3 m-md-3 p-3 box text-center <?php the_sub_field('class'); ?>">
                <i class="icon d-block p-5"></i>
                <h2 class="box-title text-upper mt-3">
                  <?php  // display a sub field value
                    the_sub_field('title'); ?>
                </h2>
                <p class="mt35"><?php  // display a sub field value
                    the_sub_field('text'); ?>
                </p> 
                <a href="<?php the_sub_field('button_link'); ?>"  class="btn btn-yellow w-90"><?php the_sub_field('button_text'); ?></a> 
                <?php if(get_sub_field('footer_text')) { ?>
                  <div class="footer_text">
                    <?php the_sub_field('footer_text'); ?>
                  </div>
                <?php } ?>
              </div>

             <?php endwhile;

        endif;

      ?>
    </div>
  </div><!-- /.container -->
</div>
