<div class="intro container">
  <div class="row">
    <div class="mt-5 mb-5 col-md-5 offset-md-6">
      <h1 class="section-title">ZORGELOOS VERHUIZEN MET ERVAREN VERHUISSPECIALIST</h1>
      <p class="mb-5">Gaat u verhuizen? Bij Verhuizingen DAC beleeft u een snelle en stressvrije verhuis in binnen- en buitenland, particulieren en professionals. Als erkend en ervaren verhuisspecialist bent u bij ons zeker van de beste service en professionele coördinatie bij het verhuizen, het verhuur van onze bestel- of aanhangwagens of geniet u van onze gespecialiseerde infrastructuur.</p>
      <p><strong>Waarom verhuizen met Verhuizingen DAC?</strong></p>
      <ul class="check-list">
        <li>Professionele verhuisservice voor particulieren én bedrijven</li>
        <li>Het juiste verhuistransport waaronder gespecialiseerde verhuisliften</li>
        <li>Meubelbewaring: veilige opslag in onze eigen loodsen en containers</li>
        <li>Ruim aanbod verhuisdozen, verhuisdekens en ander verpakkingsmateriaal</li>
      </ul>
      <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow calculator mt-3">BEREKEN DE PRIJS VAN JE VERHUIS</a>
      <p class="mt-3 mb-3 extra-text blue-color"><a href="<?php echo site_url(); ?>/contact/">Of contacteer ons voor meer informatie</a></p>
    </div>
    <div class="col-md-6 mt-md-5">
      <h2 class="section-subtitle">Expert in verhuizen - 24/7 bereikbaar</h2>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-md-6">
      @php /*echo get_field('intro_text_2') */@endphp
      <p>U wilt als particulier of bedrijf verhuizen? Ons ervaren team van verhuizers staat 7 dagen op 7 voor u klaar. Bovendien bent u steeds zeker van een eerlijke en vaste prijs, waarbij u de eerste 50 km vervoerskosten gratis krijgt. Via onze <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/">handige calculatortool</a> berekent u zelf uw</p>
      <p>Met meer dan 18 jaar ervaring en talloze <a href="<?php echo site_url(); ?>/referenties/">referenties</a> in verhuizen bent u bij Verhuizingen DAC zeker van een zorgeloze verhuis: van administratie en transport tot de meubelmontage in uw nieuwe stek. Heeft u nog verhuisdozen nodig? De eerste tien stuks krijgt u van ons cadeau! </p> 
      <p>Ook verhuizen met een erkende verhuisfirma?</p>
      <a href="<?php echo site_url(); ?>/contact/" class="btn btn-red calculator doc mt-3 ml-3">Contacteer onze experts</a>
    </div>
    <div class="col-md-6">
      @php /*echo get_field('intro_text_3') */@endphp
      <p>Wilt u als particulier of bedrijf verhuizen? Met talloze tevreden klanten bent u bij Verhuizingen DAC zeker van een zorgeloze verhuis van uw (kantoor)meubelen voor een zachte prijs. Onze ervaren verhuizers bezorgen u 7 dagen op 7 een gemakkelijke verhuis: van administratie en transport tot de meubelinstallatie in uw nieuwe stek.</p>
      <p><strong>Verhuizen met Verhuizingen DAC</strong></p>
      <ul class="check-list">
        <li>garandeert u een professionele verhuisservice</li>
        <li>verzekert het juiste verhuistransport zoals een <a href="<?php echo site_url(); ?>/liftservice/">verhuislift</a></li>
        <li>zorgt optioneel voor <a href="<?php echo site_url(); ?>/meubelbewaring/">meubelbewaring</a> met veilige opslag</li>
        <li>biedt u ook <a href="<?php echo site_url(); ?>/verhuisdozen/">verhuisdozen</a>, -dekens en ander verpakkingsmateriaal indien gewenst</li>
      </ul>
      <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow calculator mt-3">BEREKEN DE PRIJS VAN JE VERHUIS</a>
      <p class="mt-3 mb-3 extra-text blue-color"><a href="<?php echo site_url(); ?>/contact/">Of contacteer ons voor meer informatie</a></p>
    </div>
  </div>
</div>
<div id="belgie" class="bg-white">
  <div class="container">
    <div class="row pb-5">
      <div class="mt-5 col-md-12">
        <h1 class="section-title mb-4">SNEL EN SIMPEL VERHUIZEN IN BELGIË</h1>
      </div>
      <div class="col-md-6">
        <h2 class="section-subtitle">Stressvrij Verhuizen in Limburg of België</h2>
        <p>U woont in Vlaanderen en wil verhuizen naar Wallonië? Dat kan. Ook over de taalgrens is Verhuizingen DAC actief. Wij verhuizen over heel het land. <a href="<?php echo site_url(); ?>/contact/">Contacteer een van onze contactpersonen in uw buurt</a> en wij regelen uw complete verhuis binnen België. </p>
      </div>
      <div class="col-md-6">
        <h2 class="section-subtitle">Voordelig verhuizen bij Verhuizingen DAC</h2>
        <p>Bij Verhuizingen DAC gebeurt uw verhuis niet alleen veilig en vlot, maar ook aan een voordelige prijs. U krijgt de garantie van een eerlijke en vaste prijs, waarbij u de eerste 50 km vervoerskosten gratis krijgt. Goedkoop en vakkundig verhuizen? Ga dan voor onze populaire budgetformule: drie uur lang ervaren vakmanschap met professioneel verhuismateriaal. Zoekt u nog verpakking of <a href="<?php echo site_url(); ?>/verhuisdozen/">verhuisdozen</a>? Doe dan opnieuw een koopje. De eerste tien dozen krijgt u gratis!</p>
      </div>
      <div class="col-md-6">
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow calculator mt-5 text-upper">Vraag uw gratis verhuisofferte aan huis </a>
        <p class="mt-3 mb-3 extra-text blue-color">Nog niet overtuigd? Bel ons op 011 60 63 43 of bekijk onze <a href="<?php echo site_url(); ?>/referenties/">referenties</a>.</p>
      </div>
      <div class="col-md-6 mt-5 mt-md-0">
        <img src="{{ get_stylesheet_directory_uri() }}/assets/images/verhuizingen/truck.png"  class="img-fluid" width="440" />
      </div>
    </div>
  </div>
</div>
<div id="buitenland" class="trans-intern">
  <div class="container">
    <div class="row pb-5">
      <div class="mt-5 mb-5 col-md-12">
        <h1 class="section-title">Probleemloos verhuizen naar het buitenland met Verhuizingen DAC</h1>
      </div>
      <div class="col-md-6 mb-5">
        <p>Verhuizen naar het buitenland wordt bij Verhuizingen DAC een gemakkelijke klus. Vergeet uw administratieve, opslag- en transportzorgen. Wij nemen de praktische kant van uw internationale verhuis volledig in handen: van het regelen van douane- en verzekeringsformaliteiten tot het transport ter plaatse. Zo stapt u ontspannen de wagen of het vliegtuig in. Op uw eindbestemming staan uw bezittingen gewoon voor u klaar. Zo kan u zorgeloos uw nieuwe avontuur in het buitenland starten. </p>
        <p>Als ervaren internationale verhuisspecialist houdt verhuizen naar het buitenland voor ons geen geheimen meer in. Voor een totaalcoördinatie van verhuizingen binnen of buiten Europa kan u steeds bij ons terecht.</p>
      </div>
      <div class="col-md-6 mb-5">
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow text-upper mb-3">Ik wil verhuizen naar het buitenland</a>
        <p class="mb-3 extra-text blue-color"><a href="#verhuistips">Of bekijk onze handige verhuistips</a></p>
        <p>Heeft u in afwachting van uw verhuis naar het buitenland extra opslagruimte nodig, dan kan u hiervoor ook bij ons terecht. Uw <a href="<?php echo site_url(); ?>/meubelbewaring/">meubelbewaring</a> gebeurt steeds in afgesloten, vochtvrije ruimtes die u voor korte of lange periode kan huren. </p>
      
      </div>
      <img src="{{ get_stylesheet_directory_uri() }}/assets/images/verhuizingen/map.svg"  class="img-fluid map" />
      <div class="col-md-6 mt-5">
        <h2 class="section-subtitle">Verhuizen naar het buitenland: binnen Europa</h2>
        <p>Wekelijks helpen wij diverse klanten verhuizen naar het buitenland. Wilt u een nieuw avontuur starten in Europa? Wij voorzien steeds het papierwerk van A tot Z (verzekering en douane), regelen het transport over land of zee en helpen u ter plaatse met de montage. Verhuist u bovendien naar Frankrijk of Spanje, dan geniet u van een voordeeltarief dankzij onze slimme combiverhuis. Onze uitstekende service en ervaren internationale verhuizers krijgt u er gratis bij, ongeacht de bestemming.</p>
        <h2 class="section-subtitle mt-5">Verhuizen naar het buitenland: buiten Europa en overzee</h2>
        <p>Voor een verhuis buiten Europa en naar overzeese bestemmingen kan u eveneens vertrouwen op onze jarenlange ervaring en expertise. Verhuizen naar het buitenland gebeurt met de bestelwagen (rijbewijs B) die u bij ons kan huren of per zeecontainer voor overzeese bestemmingen. Ook vrachtvliegtuigen worden in sommige gevallen (moeilijker te bereiken bestemmingen) ingezet. </p>
      </div>
      <div class="col-md-6 mt-5">
        <h3 class="section-subtitle-h3">Uw spullen veilig verhuizen naar het buitenlandper zeecontainer</h3>
        <p>Geen transport is veiliger voor uw spullen dan dat met een zeecontainer. Uw meubilair, machines of kostbare voorwerpen zitten veilig weggeborgen in stalen containers. Elk stuk is zelfs afzonderlijk ingepakt voor de beste veiligheid. Eens over zee doet gespecialiseerd wegtransport de rest. Het bijbehorende papierwerk vullen wij in uw plaats in. Alweer een last bespaard! </p>
        <p>Waar ook ter wereld, uw verhuis naar het buitenland is bij ons in goede handen. Wij verhuizen veilig uw meubilair, machines en kostbare voorwerpen, van Europa tot Amerika en Azië. Voor vragen of gespecialiseerd advies over verhuizen naar het buitenland kan u terecht op het nummer +32 (0)11 60 63 43. </p>
        <a href="<?php echo site_url(); ?>/contact/" class="btn btn-yellow text-upper mt-3">Contacteer onze verhuisexperts</a>
        <p class="mt-3 mb-3 extra-text blue-color"><a href="<?php echo site_url(); ?>/calculator/calculator-pakket/">Of bereken uw internationale verhuisprijs</a></p>
        <img src="{{ get_stylesheet_directory_uri() }}/assets/images/dac-container.png"  class="img-fluid" width="400" />
      </div>
    </div>
  </div>
</div>
<div id="verhuistips" class="bg-white tips">
  <div class="container">
    <div class="row pb-5">
      <div class="mt-5 col-12">
        <div class="row">
          <div class="col-md-8">
            <h1 class="section-title">VERHUISTIPS VOOR EEN ZORGELOZE EN VLOTTE VERHUIS</h1>
            <p>Indien u zich niet voldoende en tijdig voorbereidt, kan verhuizen een stressvolle bedoening worden. Wij helpen u graag op weg met onze handige verhuistips, want een voorbereid verhuizer is er twee waard!</p>
            <p>Hulp nodig bij uw verhuis of graag meer verhuistips?</p>
            <a href="<?php echo site_url(); ?>/contact/" class="btn btn-yellow calculator mb-3">Contacteer ons</a>
            <p class="extra-text blue-color"><a href="tel: +3211606343">Of bel ons op +32 (0)11 60 63 43</a></p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <h2 class="tip-title">Verhuistip 1: Begin tijdig</h2>
        <p>Bij een verhuis komt altijd meer kijken dan eerst gedacht. Een tijdige voorbereiding, een ruime voorraad <a href="<?php echo site_url(); ?>/verhuisdozen/">verhuisdozen</a> en/of een <a href="<?php echo site_url(); ?>/meubelbewaring/">meubelopslagruimte</a> zijn dan ook belangrijk. Probeer elke dag enkele verhuisdozen in te pakken. Zo vermijd je last-minute verhuisstress!</p>
        <h2 class="tip-title">Verhuistip 2: Houd kamers gescheiden</h2>
        <p>Gebruik voor elke kamer aparte verhuisdozen. Zo behoudt u het overzicht en vindt u bij het uitpakken uw spullen snel en eenvoudig terug. </p>
        <h2 class="tip-title">Verhuistip 3: Voor elke verhuisdoos een nummer</h2>
        <p>Door elke doos een nummer te geven en dit toe te kennen aan een bepaalde kamer, bespaart u bij het uitpakken heel wat tijd. Eventuele verhuishulp weet bijgevolg ook meteen naar welke kamer de verhuisdoos gebracht mag worden.</p>
        <h2 class="tip-title">Verhuistip 4: Pak lampen op de juiste manier in</h2>
        <p>Om schade te vermijden bij het verhuizen, draait u best de gloeilamp uit de lampenkap en wikkelt u deze laatste in keukenrol in plaats van krantenpapier. Plak losse elementen, zoals decoratieve knoppen of schakelaars, stevig vast om stukken te vermijden.  </p>
      </div>
      <div class="col-md-4">
        <h2 class="tip-title">Verhuistip 5: Elektronische apparaten</h2>
        <p>Waardevolle elektronica zoals uw computer en tv-toestel verpakt u best in hun originele verpakking. Heeft u deze niet meer? Wikkel uw elektronische apparaten dan in beschermende folie en plaats dekens of lakens in de <a href="<?php echo site_url(); ?>/verhuisdozen/">verhuisdoos</a> zodat de voorwerpen stevig vastzitten. De snoeren doet u in een aparte plastic zak. Probeer aan te duiden waarvoor elk snoer dient om vergissingen achteraf te vermijden. </p>
        <h2 class="tip-title">Verhuistip 6: Opgepast met brandbare of licht ontvlambare vloeistoffen</h2>
        <p>Brandbare of licht ontvlambare vloeistoffen en spuitbussen mag u niet inpakken en houdt u best apart. Verhuist u bijvoorbeeld <a href="#buitenland">naar warmere of hoger gelegen oorden</a>? Dan kan de verandering in temperatuur en druk lekkage of zelfs explosie veroorzaken. </p>
        <h2 class="tip-title">Verhuistip 7: Kleding, gordijnen en tapijten</h2>
        <p>Schade aan uw kostbare stoffen zoals gordijnen en tapijten vermijdt u door ze in beschermfolie te wikkelen of ze in herbruikbare zakken te steken. Voor uw kledij bestaan er aangepaste <a href="<?php echo site_url(); ?>/verhuisdozen/">verhuisdozen</a> (garderobedozen). Deze zijn voorzien van een kleerhanger en zijn extra stevig. Zo komen uw maatpakken en hemden kreukvrij uit de verhuis. </p>
      </div>
      <div class="col-md-4">
        <h2 class="tip-title">Verhuistip 8: Stop niet te veel medicijnen in één doos</h2>
        <p>Propt u alle medicijnen bijeen, dan kan dit schade veroorzaken. Verwijder preventief ook breekbare flesjes en voorwerpen die kunnen lekken. Doe medicijnkastjes op slot en houd de sleutel in de handbagage.</p>
        <h2 class="tip-title">Verhuistip 9: Pak breekbare spullen voorzichtig in</h2>
        <p>Wikkel spiegels, lijsten en schilderijen nauwkeurig in beschermfolie en plaats ze verticaal in een stevige verhuisdoos. Andere breekbare spullen, zoals glaswerk, kopjes en borden beschermt u door ze in papier, oude kledij of handdoeken te rollen. Voor tafelservies gebruikt u best keukenrol. </p>
        <p>Bij het plaatsen van uw breekbare spullen in de verhuisdozen, zet u de zwaardere spullen onderaan en flessen rechtop. Scherpe zaken plaatst u best in een aparte doos om scherven te vermijden, ook al brengen ze geluk. </p>
        <h2 class="tip-title">Verhuistip 10: Ook dat nog</h2>
        <p>Verhuis de wasmachine alleen met vastgezette trommel en zet kleine planten bij elkaar in een verhuisdoos. Belangrijke papieren plaatst u in de handbagage. Om een vlotte verhuis te garanderen, houdt u best alle sleutels, montagegereedschap en -handleidingen bij elkaar. Zo kan u op een georganiseerde manier beginnen uitpakken. </p>
      </div>
    </div>
  </div>
</div>
