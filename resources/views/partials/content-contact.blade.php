<div class="contact container mt-5 mb-5">
   <div class="text-center mb-5 w-80 ml-auto mr-auto">
      <h1 class="section-title">Verhuizingen DAC: uw verhuisfirma in Limburg</h1>
      <p>Heeft u een vraag over <a href="<?php echo site_url(); ?>/verhuizen/#belgie">binnen</a>- of <a href="<?php echo site_url(); ?>/verhuizen/#buitenland">buitenlandse</a> verhuizingen, bent u op zoek naar stevige <a href="<?php echo site_url(); ?>/verhuisdozen/">verhuisdozen</a> of een ruimte voor uw <a href="<?php echo site_url(); ?>/meubelbewaring/">meubelbewaring</a>? Als ervaren verhuisfirma in Limburg helpen wij u graag verder. Vul onderstaand formulier in en vraag het aan onze verhuisexperts. </p>
      <h2 class="section-subtitle mt-5">NEEM CONTACT MET ONS OP</h2>
      <p>Vul onderstaand formulier in en klik op "Verzenden"</p>
    </div>
  <div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="row">
        <div class="address col-md-6">
          <h3 class="section-title">DAC VERHUIZINGEN</h3>
          <dl class="row mb-5">
            <dt class="col-3">Brussel:</dt><dd class="col-9">02 502 87 83</dd>
            <dt class="col-3">Antwerpen:</dt><dd class="col-9">03 225 30 01</dd>
            <dt class="col-3">Leuven:</dt><dd class="col-9">016 20 50 72</dd>
            <dt class="col-3">Tongeren:</dt><dd class="col-9">012 23 39 81</dd>
            <dt class="col-3">Lanaken:</dt><dd class="col-9">089 41 72 09</dd>
          </dl>
          
          <div class="location-details mb-5">
            <h4 class="section-subtitle">Hoofdvestiging</h4>
            <span class="d-block w-100 location">Kempische Steenweg 471A – 3500 Hasselt</span>
            <span class="d-block w-100 phone">@php the_field('contact_phone', 'option'); @endphp</span>
            <span class="d-block w-100 email">@php the_field('contact_email', 'option'); @endphp</span>
          </div>
          <div class="location-details mb-5">
            <h4 class="section-subtitle">Meubelbewaring</h4>
            <span class="d-block w-100 location">Fabriekstraat 32 – Sint-Truiden</span>
            <span class="d-block w-100 email">@php the_field('contact_email', 'option'); @endphp</span>
          </div>
          <h3 class="section-title">OPENINGSUREN</h3>
          <dl class="row">
            <dt class="col-3">Maandag</dt><dd class="col-9">09:00 - 18:00</dd>
            <dt class="col-3">Dinsdag</dt><dd class="col-9">09:00 - 18:00</dd>
            <dt class="col-3">Woensdag</dt><dd class="col-9">09:00 - 18:00</dd>
            <dt class="col-3">Donderdag</dt><dd class="col-9">09:00 - 18:00</dd>
            <dt class="col-3">Vrijdag</dt><dd class="col-9">09:00 - 18:00</dd>
            <dt class="col-3">Zaterdag</dt><dd class="col-9">09:00 - 18:00</dd>
            <dt class="col-3">Zondag</dt><dd class="col-9">Gesloten</dd>
          </dl>
        </div>
        
        <div class="col-md-6 form">
          @php echo do_shortcode('[contact-form-7 id="123" title="Contact form 1"]'); @endphp
        </div>
      </div>
    </div>
  </div>
</div>
