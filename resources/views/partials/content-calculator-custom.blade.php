<div class="intro calculator-wrap container mb-5">
  <div class="row">
    <div class="mt-5 mb-5 col-md-8 offset-md-2 text-center">
      <h1 class="section-title "><?php the_title(); ?></h1>
      <p>Stel hieronder zelf een pakket samen.</p>
      <div class="boxes calculator-custom">
        <div id="pakket1" class="box w-100 mt-5">
          <span class="number mb-4">1</span>
          <h3 class="section-subtitle text-upper mb-4">Verhuiswagen + PERSONEEL + LIFT</h3>
          <div id="selectVerhuizers" class="d-flex justify-content-center select">
              <div class="item col" data-select="2">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/people-2.jpg"  class="img-fluid" />
              </div>
              <div class="item col" data-select="3">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/people-3.jpg"  class="img-fluid" />
              </div>
              <div class="item col" data-select="4">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/people-4.jpg"  class="img-fluid" />
              </div>
          </div>
          <p class="mt-4 pb-4">Kies het aantal verhuizers, 2, 3 of 4.</p>
        </div>
        <div id="pakket2" class="box w-100 mt-5 pb-4">
          <span class="number mb-4">2</span>
          <h3 class="section-subtitle text-upper mb-4">HOEVEEL EXTRA UREN VOORZIET U?</h3>
          <p class="text-center">(3 uur zijn inbegrepen vanaf vertrek tot aankomst magazijn<br /> Kempische Steenweg 471 A te Hasselt)</p>
          <div class="form-group contact w-80 ml-auto mr-auto">
            <label> 
              <span class="wpcf7-form-control-wrap email">
                <input id="uren" type="text" name="hours" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="Extra uren">
              </span> 
            </label>
          </div>
        </div>
        <div id="pakket3" class="box w-100 mt-5">
          <span class="number mb-4">3</span>
          <h3 class="section-subtitle text-upper mb-4">LIFT + PERSONEEL</h3>
          <p>Gratis verplaatsing in de Benelux</p>
          <div id="selectTruck" class="d-flex justify-content-center mb-4 select">
            <div class="item col" data-select="Truck-20m3">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/truck-20m3.jpg"  class="img-fluid" />
              </div>
              <div class="item col" data-select="Truck-40m3">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/truck-40m3.jpg"  class="img-fluid" />
              </div>
            </div>
            <div id="selectLift" class="d-flex justify-content-center pb-4 select">
              <div class="item pb-0 col" data-select="1-5 verdieping">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/lift.jpg"  class="img-fluid" />
                <p class="bottom mb-0">1e t/m de 5de verdieping</p>
              </div>
              <div class="item pb-0 col" data-select="6+ verdieping">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/lift.jpg"  class="img-fluid" />
                <p class="bottom mb-0">Vanaf de 6de verdieping</p>
              </div>
          </div>
        </div>
        <div id="pakket2" class="box w-100 mt-5 pb-4">
          <span class="number mb-4">4</span>
          <h3 class="section-subtitle text-upper mb-4">Hoeveel extra kilometers voorziet u?</h3>
          <p class="text-center"></p>
          <div class="form-group contact w-80 ml-auto mr-auto">
            <label> 
              <span class="wpcf7-form-control-wrap email">
                <input id="extraKM" type="text" name="km" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="KM">
              </span> 
            </label>
          </div>
        </div>
        <div id="pakket2" class="box w-100 mt-5 pb-4">
          <span class="number mb-4">5</span>
          <h3 class="section-subtitle text-upper mb-4">U verhuist Van... Naar...</h3>
          <p class="text-center"></p>
          <div class="form-group contact w-80 ml-auto mr-auto">
            <label> 
              <span class="wpcf7-form-control-wrap email">
                <input id="dirFrom" type="text" name="from" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="Van..">
              </span> 
            </label>
          </div>
          <div class="form-group contact w-80 ml-auto mr-auto">
            <label> 
              <span class="wpcf7-form-control-wrap email">
                <input id="dirTo" type="text" name="to" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="Naar..">
              </span> 
            </label>
          </div>
        </div>
      </div>
      <div class="contact">
        <div class="form text-left">
          @php echo do_shortcode('[contact-form-7 id="179" title="Contact Custom"]'); @endphp
        </div>
      </div>
    </div>
  </div>
</div>
<img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/pre-footer-image.jpg"  class="img-fluid w-100 mt-3" />
<script>
  jQuery(function($) {
    $(document).on('click', '.select > .item', function(){ 
      $(this).removeClass('notc').addClass('clicked').siblings('.item').removeClass('clicked').addClass('notc');
      if($(this).parent().attr('id') == 'selectVerhuizers') {
        $('input#verhuizers').val($(this).attr('data-select'));
      } else if($(this).parent().attr('id') == 'selectTruck') {
        $('input#truck').val($(this).attr('data-select'));
      } else if($(this).parent().attr('id') == 'selectLift') {
        $('input#lift').val($(this).attr('data-select'));
        $('input#liftValue').val('1');
      }
    })
    $('#uren').on('input', function() {
      $('input#extraUren').val($(this).val());
    });
    $('#extraKM').on('input', function() {
      $('input#km').val($(this).val());
    });
    $('#dirFrom').on('input', function() {
      $('input#van').val($(this).val());
    });
    $('#dirTo').on('input', function() {
      $('input#naar').val($(this).val());
    });
  })
   
</script>
