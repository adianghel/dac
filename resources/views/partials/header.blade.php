<header class="banner nav-container">
  <div class="container">
    <div class="row">
      <a class="brand col-lg-2 pr-0" href="{{ home_url('/') }}">
        <img src="{{ get_stylesheet_directory_uri() }}/assets/images/logo.svg"  class="img-fluid d-none d-md-block" alt="DAC" width="226" />
        <img src="{{ get_stylesheet_directory_uri() }}/assets/images/logo-mobile.svg"  class="img-fluid d-block d-md-none" alt="DAC" width="90" />
      </a>
      <span class="mobileSlogan text-upper text-center d-block d-md-none">Verhuizingen & Opslag</span>
      <button class="navbar-toggler d-lg-none hamburger hamburger--vortex" type="button" data-toggle="collapse" data-target="#headerNav" aria-controls="headerNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="hamburger-box">
        <span class="hamburger-inner"></span>
        </span>
      </button>
      <div id="hNavbar" class="col-lg-10 pl-0 navbar-expand-lg">
        <div class="info-bar align-self-end text-right mb-lg-3">
          <img src="{{ get_stylesheet_directory_uri() }}/assets/images/icon-Erkend-Verhuizer.svg" width="70" class="d-none d-lg-inline-block" />
          <div class="red-bg d-inline">
            <a href="tel:<?php echo get_field('contact_phone', 'option'); ?>" class="phone contact-link align-self-center">@php the_field('contact_phone', 'option'); @endphp</a>
            <a href="mailto:<?php the_field('contact_email', 'option'); ?>" class="email contact-link align-self-center">@php the_field('contact_email', 'option'); @endphp</a>
          </div>
          <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow offerte contact-link align-self-center mr-0">Gratis offerte</a>
        </div>
        <nav class="nav-primary align-self-end justify-content-lg-end collapse navbar-collapse" id="headerNav">
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav flex-column flex-md-row justify-content-end']) !!}
          @endif
        </nav>
      </div>
    </div><!-- /.row -->
  </div>
</header>
