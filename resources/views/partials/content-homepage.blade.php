@php the_content() @endphp
<div class="section statistics">
  <div class="container">
    <div class="col-md-8 offset-md-2">
      <h3 class="text-center text-upper pt-5 section-title"><?php echo get_field('statistics_box_title'); ?></h3>
      <p class="text-center"><?php echo get_field('statistics_box_text'); ?></p>

      <div class="d-flex justify-content-center">
        <div class="col-4 p-3 box text-center move-year">
          <i class="icon d-block"></i>
          <h3 class="text-upper"><span class="num" id="count-moves">2000</span>
          verhuizingen per jaar
          </h3>
        </div>
        <div class="col-4 p-3 box text-center move-intern">
          <i class="icon d-block"></i>
          <h3 class="text-upper"><span class="num" id="count-intern">100</span>
            INTERNATIONALE verhuizingen per jaar
          </h3>
        </div>
        <div class="col-4 p-3 box text-center move-days">
          <i class="icon d-block"></i>
          <h3 class="text-upper">
            <span class="num" id="count-days">7</span>
              DAGEN OP 7 BESCHIKBAAR
          </h3>
        </div>
      </div>
      <div class="text-center">
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-red calculator mt-3 text-upper">Bereken uw verhuisprijs</a>
      </div>
      <div class="text-center mt-3">
        <a href="<?php echo site_url(); ?>/contact/" class="w-color">Hulp nodig? Wij helpen u graag verder</a>
      </div>
    </div>
  </div><!-- /.container -->
</div>
<div class="section benefits">
  <div class="shape absolute"></div>
  <div class="container">
    <div class="col-md-10 offset-md-1">
      <div class="row">
        <div class="col-md-6 truck pr-md-5">
          <img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/DAC-NV_wagen.png"  class="img-fluid" />
        </div>
        <div class="col-md-6 pl-md-5">
          <h2 class="text-upper">Uw vaste verhuisfirma in Limburg</h2>
          <p>Verhuizen in binnen- of buitenland? Een verhuislift of verhuiswagen huren? Verhuisdozen of ander verpakkingsmateriaal bestellen? Waar u ook naar op zoek bent, bij Verhuizingen DAC vindt u gegarandeerd wat u zoekt. U kan ons 24/7 bereiken. Ons team ervaren verhuizers staat voor u klaar.</p>
          <a href="<?php echo site_url(); ?>/contact/" class="btn btn-yellow calculator box mt-2 mb-5 text-center text-upper">MAAK EEN AFSPRAAK</a>
        </div>
        <div class="col-md-6 pr-md-5">
           <h2 class="text-upper mt-5">Uw complete verhuisfirma</h2>
            <ul>
              <li>Veilig en verzekerd verhuizen in binnen- of buitenland</li>
              <li>Voor particulieren en bedrijven</li>
              <li>Uitgebreid wagenpark en gespecialiseerde liftservice</li>
              <li>Veilige meubelbewaring</li>
              <li>Ruim gamma verhuisdozen en -materiaal</li>
            </ul>
            <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow calculator mt-3">VERHUIS CALCULATOR</a>
        </div>
        <div class="col-md-6 pl-md-5 mt-5 mt-md-0">
          <img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/DACnv_verhuismateriaal.png" width="310"  class="img-fluid" />
          <div class="text-right">
            <img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/arrow-material.png"  class="img-fluid mt-2" width="220" />
          </div>
        </div>
      </div>
    </div>
  </div><!-- /.container -->
</div>
<div class="section testimonials mt-5">
  <div class="container">
    <div class="col-md-6 offset-md-3">
      <div class="text-center">
        <h2 class="text-upper">Wat zeggen onze klanten?</h2>
        <p>Verhuizingen DAC is uw verhuisfirma in Limburg waar u 24/7 terechtkan. We verhuizen meer dan 2.000 klanten per jaar. Zij delen graag hun mening met u.</p>
      </div>
    </div>
    <div class="mt-5 pt-5">
      <div class="row justify-content-center">
      <?php
      
      if( have_rows('boxes') ):
        $testimonial_boxes = get_field('boxes');
        shuffle($testimonial_boxes);
        $random_boxes = array_slice($testimonial_boxes, 0, 3);
        foreach ($random_boxes as $box) : ?>
          <blockquote class="col-md-4 p-3 box text-center justify-content-center d-flex flex-column">
            <img src="<?php echo  $box['box_photo']; ?>"  class="img-fluid" />
            <cite class="author d-block"><?php  echo $box['box_name']; ?></cite>
            <cite class="source d-block"><?php echo  $box['box_service']; ?></cite>
            <div class="d-flex align-items-center flex-grow-1">
              <p class="mt-3"><?php echo  $box['box_text']; ?></p>
            </div>
            <span class="d-block stars <?php echo  $box['stars']; ?>"></span>
          </blockquote>
        <?php endforeach; endif; ?>
      </div>
    </div>
    <p class="text-center mt-3 mb-3 extra-text blue-color">Nog meer horen? Bekijk ook onze <a href="<?php echo site_url(); ?>/referenties/">andere referenties van verschillende projecten</a></p>
    <div class="benefits-2 text-center mb-5">
      <img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/icons-benefits2.svg"  class="img-fluid" width="600" />
    </div>
  </div><!-- /.container -->
</div>
<img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/pre-footer-image.jpg"  class="img-fluid w-100 mt-3" />