<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  @php wp_head() @endphp
   <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T9DFCKW');</script>
<!-- End Google Tag Manager -->
<?php if(is_page(124)) { ?>
  <script>
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      ga('send', 'event', 'Contact Form', 'submit');
    }, false );
  </script>
<?php } else if(is_page(169)) { ?>
  <script>
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      ga('send', 'event', 'Calculator Pakketen Form', 'submit');
    }, false );
  </script>
<?php } else if(is_page(166)) { ?>
  <script>
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      ga('send', 'event', 'Calculator Custom Form', 'submit');
    }, false );
  </script>
<?php } ?>
</head>
