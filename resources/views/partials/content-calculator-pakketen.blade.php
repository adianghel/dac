<div class="intro calculator-wrap container mb-5">
  <div class="row">
    <div class="mt-5 mb-5 col-md-8 offset-md-2 text-center">
      <h1 class="section-title "><?php the_title(); ?></h1>
      <p>@php echo get_field('page_subtitle'); @endphp </p>
      <div class="boxes">
        <div id="pakket1" class="box w-100 mt-5">
          <span class="number mb-4">1</span>
          <h3 class="section-subtitle text-upper mb-4">Verhuiswagen + PERSONEEL + LIFT</h3>
          <div class="d-flex justify-content-center">
              <div class="item truck">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/truck-20m3.jpg"  class="img-fluid" />
              </div>
              <div class="item people">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/people-2x.jpg"  class="img-fluid" />
              </div>
              <div class="item lift">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/lift.jpg"  class="img-fluid" />
              </div>
          </div>
          <p class="mt-4 bottom">Dit pakket is geschikt voor verhuizingen <strong>t/m de 5de verdieping</strong></p>
        </div>
        <a href="<?php echo site_url(); ?>/calculator/calculator-custom/" class="btn btn-yellow text-center text-upper">(OF) ZELF EEN PAKKET SAMENSTELLEN</a>
        <div id="pakket2" class="box w-100 mt-5">
          <span class="number mb-4">2</span>
          <h3 class="section-subtitle text-upper mb-4">Verhuiswagen + PERSONEEL + LIFT</h3>
          <div class="d-flex justify-content-center">
              <div class="item truck">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/truck-20m3.jpg"  class="img-fluid" />
              </div>
              <div class="item people">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/people-2x.jpg"  class="img-fluid" />
              </div>
              <div class="item lift">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/lift.jpg"  class="img-fluid" />
              </div>
          </div>
          <p class="mt-4 bottom">Dit pakket is geschikt voor verhuizingen <strong>vanaf de 6de verdieping</strong></p>
        </div>
        <a href="<?php echo site_url(); ?>/calculator/calculator-custom/" class="btn btn-yellow text-center text-upper">(OF) ZELF EEN PAKKET SAMENSTELLEN</a>
        <div id="pakket3" class="box w-100 mt-5">
          <span class="number mb-4">3</span>
          <h3 class="section-subtitle text-upper mb-4">LIFT + PERSONEEL</h3>
          <p class="c-red">Gratis verplaatsing in de Benelux</p>
          <div class="d-flex justify-content-center">
              <div class="item lift">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/lift.jpg"  class="img-fluid" />
              </div>
              <div class="item people">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/people-1x.jpg"  class="img-fluid" />
              </div>
          </div>
          <p class="mt-4 bottom">Dit pakket is geschikt voor verhuizingen <strong>t/m de 5de verdieping</strong></p>
        </div>
        <a href="<?php echo site_url(); ?>/calculator/calculator-custom/" class="btn btn-yellow text-center text-upper">(OF) ZELF EEN PAKKET SAMENSTELLEN</a>
        <div id="pakket4" class="box w-100 mt-5">
          <span class="number mb-4">4</span>
          <h3 class="section-subtitle text-upper mb-4">LIFT + PERSONEEL</h3>
          <p class="c-red">Gratis verplaatsing in de Benelux</p>
          <div class="d-flex justify-content-center">
              <div class="item lift">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/lift.jpg"  class="img-fluid" />
              </div>
              <div class="item people">
                <img src="{{ get_stylesheet_directory_uri() }}/assets/images/calculator/people-1x.jpg"  class="img-fluid" />
              </div>
          </div>
          <p class="mt-4 bottom">Dit pakket is geschikt voor verhuizingen <strong>vanaf de 6de verdieping</strong></p>
        </div>
      </div>
      <a href="<?php echo site_url(); ?>/calculator/calculator-custom/" class="btn btn-yellow text-center text-upper">(OF) ZELF EEN PAKKET SAMENSTELLEN</a>
      <div class="contact">
        <div class="form text-left">
          @php echo do_shortcode('[contact-form-7 id="177" title="Contact Pakketen"]'); @endphp
        </div>
      </div>
    </div>
  </div>
</div>
<img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/pre-footer-image.jpg"  class="img-fluid w-100 mt-3" />
<script>
  jQuery(function($) {
    $(document).on('click', '.box', function(){ 
      $(this).addClass('clicked').siblings('.box').removeClass('clicked');
      if($(this).attr('id') == "pakket1") {
        $('input#packketen').val('Actie pakket 1');
        $('input#originalPackage').val('Actie pakket 1 (t/m de 5de verdieping)');
        $('input#verhuizers').val('2');
        $('input#lift_small').val('1');
        $('input#lift_big').val('0');
        $('input#bakwagen').val('1');
        $('input#mover_price').val('295');
      } else if ($(this).attr('id') == "pakket2") {
        $('input#packketen').val('Actie pakket 2');
        $('input#originalPackage').val('Actie pakket 2 (vanaf de 6de verdieping)');
        $('input#verhuizers').val('2');
        $('input#lift_small').val('0');
        $('input#lift_big').val('1');
        $('input#bakwagen').val('1');
        $('input#mover_price').val('325');
      } else if ($(this).attr('id') == "pakket3") {
        $('input#packketen').val('Actie pakket 3');
        $('input#originalPackage').val('Actie pakket 3 (t/m de 5de verdieping)');
        $('input#verhuizers').val('1');
        $('input#lift_small').val('1');
        $('input#lift_big').val('0');
        $('input#bakwagen').val('0');
        $('input#mover_price').val('120');
      } else if ($(this).attr('id') == "pakket4") {
        $('input#packketen').val('Actie pakket 4');
        $('input#originalPackage').val('Actie pakket 4 (vanaf de 6de verdieping)');
        $('input#verhuizers').val('1');
        $('input#lift_small').val('0');
        $('input#lift_big').val('1');
        $('input#bakwagen').val('0');
        $('input#mover_price').val('160');
      }
    })
  })
</script>
