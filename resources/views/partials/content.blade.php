<article <?php post_class('mb-5') ?>>
  <header>
    <h2 class="entry-title text-center section-subtitle mb-4"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
  </header>
  <div class="entry-summary text-center">
    <?php
      $thumb_id = get_post_thumbnail_id();
      if($thumb_id) {
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'news-thumb', true);
        $thumb_url = $thumb_url_array[0];
        ?>
        <img src="<?php echo $thumb_url ?>"class="img-fluid" />
      <?php }  ?>
      <a href="{{ get_permalink() }}" class="btn btn-red text-upper mt-4 mb-5">Lees Meer</a>
  </div>
</article>
