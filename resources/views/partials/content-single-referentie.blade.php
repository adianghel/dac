<article <?php post_class('col-md-8 offset-md-2 mt-5 news') ?>>
  <header>
    <h1 class="entry-title section-title text-center">{!! get_the_title() !!}</h1>
    {{-- @include('partials/entry-meta') --}}
    @if (get_field('subtitle'))
      <p class="page-subtitle text-center mt-5">@php echo get_field('subtitle') @endphp </p>
    @endif
  </header>
  <div class="entry-summary text-center mt-5 mb-5">
  <?php
      $thumb_id = get_post_thumbnail_id();
      if($thumb_id) {
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'news-thumb', true);
        $thumb_url = $thumb_url_array[0];
        ?>
        <img src="<?php echo $thumb_url ?>"class="img-fluid" />
      <?php }  ?>
  </div>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer class="mt-5 mb-5 text-center">
    <a href="<?php echo site_url(); ?>/referenties/" class="btn btn-red"><img src="{{ get_stylesheet_directory_uri() }}/assets/images/icon-back-arrow.png"  class="img-fluid mr-3" width="10" />Referenties</a>
  </footer>
</article>
