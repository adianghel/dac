<div class="intro calculator-wrap container mb-5">
  <div class="row">
    <div class="mt-5 mb-5 col-md-8 offset-md-2 text-center">
      <h1 class="section-title "><?php the_title(); ?></h1>
      <p>@php echo get_field('page_subtitle'); @endphp </p>
      <div class="row justify-content-center">
        <a href="<?php echo get_page_link(166); ?>" class="btn btn-white calculator mt-3 text-center text-upper">PAKKET SAMENSTELLEN</a>
        <a href="<?php echo get_page_link(169); ?>" class="btn btn-red mt-3 text-center text-upper">BESTAAND ACTIE PAKKET</a>
      </div>
    </div>
  </div>
</div>
<img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/pre-footer-image.jpg"  class="img-fluid w-100 mt-3" />
