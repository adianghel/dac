<div class="intro container">
  <div class="row">
    <div class="col-md-6 mt-5 align-self-end">
      <div class="mt-md-5">
        <h2 class="section-subtitle">Totaalcoördinatie voor elke bedrijfsverhuis</h2>
        <p>Van het zorgvuldig inpakken en vervoeren van al uw kantoorgoederen tot het terug in elkaar zetten van uw kantoormateriaal op de nieuwe bedrijfslocatie. Onze ervaren verhuizers zorgen voor een schadeloze en vlotte bedrijfsverhuis van A tot Z. </p>
        <p>Schakel de hulp in van een ervaren partner voor uw bedrijfsverhuis.</p>
        <ul class="check-list">
          <li>Voor elke organisatie: van bedrijven en fabrieken tot scholen en overheidsinstellingen</li>
          <li>Perfecte totaalcoördinatie voor en tijdens het verhuizen</li>
          <li>Uitgebreid wagenpark en gespecialiseerd verhuismateriaal</li>
          <li>Deskundig ingepakt</li>
          <li>7 dagen op 7 bereikbaar</li>
        </ul>
        <p>Uw bedrijf verhuist binnenkort? Of heeft u een specifieke verhuisvraag?</p>

        <a href="<?php echo site_url(); ?>/contact/" class="btn btn-red mt-3 mb-3 text-center text-upper">Contacteer onze verhuisexperts. </a>
        <p class="mb-3 extra-text blue-color mb-5"><a href="<?php echo site_url(); ?>/referenties/">Of bekijk onze referenties. </a></p>
      </div>
    </div>
    <div class="mt-5 mb-md-5 col-md-6">
      <h1 class="section-title mt-5">Uw bedrijfsverhuis van A-Z geregeld</h1>
      <p>Voor een ervaren en betrouwbare partner voor uw bedrijfsverhuis bent u bij Verhuizingen DAC aan het juiste adres. Wij helpen u met het verhuizen van uw burelen, kantoormeubilair en archiefkasten. Ook ziekenhuizen, fabrieken, scholen en andere instellingen kunnen bij ons terecht voor hun bedrijfsverhuis.</p>
      <h2 class="section-subtitle mt-5">Ga voor een zorgeloze bedrijfsverhuis</h2>
      <p>Bij Verhuizingen DAC bent u zeker van een bedrijfsverhuis zonder stress. Voorzien van gespecialiseerde verhuismaterialen, ervaren verhuizers en een uitgebreid wagenpark, zorgen wij er steeds voor dat uw bedrijfsverhuis schadeloos verloopt. </p>
      <p>Bevindt uw nieuwe kantoor zich op een verdieping, dan kan u bij ons een <a href="<?php echo site_url(); ?>/liftservice/">verhuislift</a> huren. Onze liften geraken probleemloos tot aan de 8e verdieping en hoger. Een ervaren liftoperator zorgt er bovendien voor dat uw bedrijfsverhuis vlot verloopt. Ook zwaardere spullen, zoals brandkasten of machines, brengen we probleemloos naar uw nieuwe locatie.</p>
      <p>Bij een bedrijfsverhuis komt uiteraard heel wat inpakwerk kijken. Dankzij ons ruim aanbod <a href="<?php echo site_url(); ?>/verhuisdozen/">verhuisdozen</a> en ander verhuismateriaal heeft u nooit opbergruimte te kort. Voor opslag op korte of lange termijn bieden we verschillende opties aan voor <a href="<?php echo site_url(); ?>/meubelbewaring/">meubelbewaring</a>. Zo blijven uw spullen in afwachting van uw bedrijfsverhuis optimaal beschermd tegen vocht, stof en diefstal. </p>
      <p>U kan eenvoudig zelf de prijs van uw bedrijfsverhuis berekenen via onze <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/">online calculator</a>. Of <a href="<?php echo site_url(); ?>/contact/">maak een afspraak</a> met een van onze verhuizers. </p>
    </div>
  </div>
</div>
