<div class="intro container">
  <div class="row">
    <div class="col-md-6 mt-5 align-self-end">
      <div class="mt-md-5">
        <p><strong>Ga voor een optimale bescherming van uw spullen dankzij onze meubelbewaring</strong></p>
        <ul class="check-list">
          <li>Inbraakveilig</li>
          <li>Droog en stofvrij</li>
          <li>Verwarmd en geventileerd</li>
          <li>Deskundig ingepakt</li>
          <li>Flexibel in huur en ruimte</li>
        </ul>
        <a href="<?php echo site_url(); ?>/contact/" class="btn btn-red mt-3 mb-3 text-center text-upper">Maak een afspraak voor uw meubelbewaring</a>
        <p class="mb-3 extra-text blue-color mb-5"><a href="<?php echo site_url(); ?>/verhuisdozen/">Of sla alvast uw voorraad verhuisdozen in</a></p>
      </div>
    </div>
    <div class="mt-5 mb-md-5 col-md-6">
      <h1 class="section-title">Meubelbewaring: voor een veilige stockage van uw spullen</h1>
      <p>Gaat u renoveren of verbouwen en weet u met uw meubels even geen blijf? <a href="<?php echo site_url(); ?>/verhuizen/">Verhuist u</a>, maar kan u nog niet meteen intrekken in uw nieuwe stulpje? Vertrekt u voor een tijdje <a href="<?php echo site_url(); ?>/verhuizen/#buitenland">naar het buitenland</a> en zoekt u wat stockageruimte?<br />
      Dan biedt onze meubelbewaring soelaas. Ook bedrijven kunnen bij ons terecht voor het stockeren van kantoormeubilair en -materiaal.</p>
      <h2 class="section-subtitle mt-5">Meubelwaring in ideale omstandigheden</h2>
      <p>Verhuizingen DAC biedt opslagruimte aan voor korte of lange termijn in Hasselt en omgeving. Met keuze uit diverse opties, meerdere volumes en variërende huurtijden biedt onze flexibele meubelbewaring een oplossing voor iedere verhuizer én voor elk budget.</p>
      <p>Bovendien mag u er zeker van zijn dat uw meubels optimaal en onder de juiste condities beschermd blijven. In onze beveiligde loods is er geen sprake van vocht en stof, is er voldoende ventilatie en steeds een optimale binnentemperatuur. Elk meubelstuk is zorgvuldig ingepakt in stevige houten boxen zodat u ze steeds in hun originele staat kan gaan ophalen. </p>
    </div>
  </div>
</div>
<div class="bg-white">
  <div class="container">
    <div class="row pb-5">
      <div class="mt-md-5 col-md-6 align-self-center">
        <h2 class="section-subtitle mb-3">Diverse opslagvolumes voor uw meubelbewaring</h2>
        <p>Of het nu gaat over een beperkt aantal meubelstukken of meerdere grote elementen, bij Verhuizingen DAC vindt u steeds een pasklare oplossing voor uw meubelbewaring. U kiest uit een opslagvolume van 5 m³ of 10 m³ en bepaalt zelf de opslagtermijn. Onze meubelbewaring werkt volgens flexibele contracten, van één dag tot een langere periode. </p>
      </div>
      <div class="col-md-6 d-flex mt-3 mt-md-5">
        <img src="{{ get_stylesheet_directory_uri() }}/assets/images/meubel/section-2-bg.jpg"  class="img-fluid align-self-center" width="440" />
      </div>
    </div>
  </div>
</div>
<div class="trans-intern">
  <div class="container">
    <div class="row pt-5">
      <div class="col-md-6 mb-5">
        <p>Wij beschikken over meer dan 10.000 m² opslagruimte (loodsen en containers) in de regio Hasselt en omstreken, waar u uw spullen veilig kan achterlaten. Bent u op zoek naar stockageruimte voor uw meubelbewaring die groter is dan 10 m³? <a href="<?php echo site_url(); ?>/contact/">Contacteer</a> onze medewerkers, zij gaan graag op zoek naar het gepaste formaat. Of reserveer hieronder een standaardformaat. </p>
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-red box mt-3 text-center text-upper">Ik kies voor een meubelbewaring van 5 m³</a>
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow box mt-4 text-center text-upper">Ik kies voor een meubelbewaring van 10 m³</a>
        <p class="extra-text blue-color mt-3">Heeft u nog wat verhuismateriaal nodig? Ook voor <a href="<?php echo site_url(); ?>/verhuisdozen/">verhuisdozen en ander inpakmateriaal</a> kan u bij Verhuizingen DAC terecht.</p>
      </div>
      <div class="col-md-6 align-self-end text-center">
          <img src="{{ get_stylesheet_directory_uri() }}/assets/images/meubel/package-delivery.jpg"  class="img-fluid mt-5" width="300" />
      </div>
    </div>
  </div>
</div>
