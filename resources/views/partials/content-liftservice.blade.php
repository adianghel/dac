<div id="introLiftservice" class="intro container">
  <div class="row mb-4 mb-md-5">
    <div class="col-md-6 mt-5 align-self-end">
      <div class="">
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-red mt-3 mb-3 text-center text-upper w-80">Ik wil een verhuislift huren</a>
        <p class="mb-3 extra-text blue-color mb-5"><a href="<?php echo site_url(); ?>/verhuizen/#verhuistips">Of lees onze verhuistips ter voorbereiding</a></p>
      </div>
    </div>
    <div class="col-md-6 mt-md-5 mb-md-5">
      <h1 class="section-title">Veilig en schadevrij verhuizen met onze verhuisliften</h1>
      <p>Wanneer meubels niet via een trap of lift op hun bestemming kunnen geraken, is een verhuislift vaak een veilige en betrouwbare oplossing. Ook voor diverse hijswerken kan u terecht bij Verhuizingen DAC. </p>
      <h2 class="section-subtitle">Verhuislift huren?</h2>
      <p>Voor <a href="<?php echo site_url(); ?>/verhuizen/">verhuizingen</a> van of naar een bepaalde verdieping, is een verhuislift onmisbaar. Ons aanbod verhuisliften biedt heel wat mogelijkheden waaruit u de ideale formule kan kiezen. Verhuizingen DAC beschikt over verhuisliften, gespecialiseerd hijsmateriaal en bestel- en aanhangwagens (rijbewijs B). </p>
      <p>Op welke verdieping u ook woont of werkt, onze verhuislift geraakt probleemloos tot aan de 8e verdieping en hoger! Een ervaren liftoperator is steeds aanwezig om alles te coördineren en ervoor te zorgen dat uw verhuis veilig, vlot en schadevrij verloopt. Een verhuislift huren kan bij Verhuizingen DAC al vanaf € 100 all-in. We bieden verschillende modellen aan zoals ladderliften en meubelliften met diverse hijscapaciteiten.</p>
    </div>
  </div>
</div>
<div id="hijswerken" class="bg-white">
  <div class="container">
    <div class="row pb-5">
      <div class="col-md-6 mt-3 mt-m5">
        <img src="{{ get_stylesheet_directory_uri() }}/assets/images/liftservice/lift-piano.png"  class="img-fluid" width="440" />
      </div>
      <div class="mt-5 col-md-6 align-self-center">
        <h2 class="section-subtitle">Hijswerken met verhuislift</h2>
        <p>Het verhuizen van delicate, kostbare stukken (bijvoorbeeld vleugelpiano’s), antiek meubilair of industriële hijsmachines vereist het werk van een ervaren verhuisprofessional. Met onze sterke verhuisliften, gespecialiseerd hijsmateriaal en aangepaste vrachtwagens, voorzien van luchtvering, garanderen we u een veilige en schadevrije verhuis. </p>
      </div>
    </div>
  </div>
</div>
<div class="trans-intern">
  <div class="container">
    <div class="row pt-4 pt-md-5">
      <div class="col-md-6 mb-5">
        <p>Probeer industriële machines uit productiehallen of magazijnen niet zelf te verplaatsen, maar doe beroep op Verhuizingen DAC. Zo vermijdt u onveilige situaties of schade en kan u meteen weer aan de slag op uw nieuwe bedrijfslocatie.</p>
        <p>Indien u nog geen toegang heeft tot de nieuwe werkplaats en een tijdelijke opslagruimte zoekt, kan u bij ons terecht voor <a href="<?php echo site_url(); ?>/meubelbewaring/">meubelbewaring</a>. Ook voor <a href="<?php echo site_url(); ?>/verhuisdozen/">verhuisdozen en ander verhuismateriaal</a> zijn wij uw aanspreekpunt. </p>
        <a href="<?php echo site_url(); ?>/calculator/calculator-pakket/" class="btn btn-yellow mt-3 mb-3 text-center text-upper w-80">Reserveer uw verhuislift</a>
      </div>
      <div class="col-md-6">
        <div class="text-center">
          <img src="{{ get_stylesheet_directory_uri() }}/assets/images/home/DAC-NV_wagen.png"  class="img-fluid" width="400" />
        </div>
      </div>
    </div>
  </div>
</div>
