{{--
  Template Name: Homepage
--}}

@extends('layouts.app-dac')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.homepage-header')
    @include('partials.content-homepage')
  @endwhile
@endsection
