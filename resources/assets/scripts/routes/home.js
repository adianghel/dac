import { CountUp } from 'countup.js';
var isInViewport = function(elem) {
  var bounding = elem.getBoundingClientRect();
  return (
    bounding.top >= 0 &&
    bounding.left >= 0 &&
    bounding.bottom <=
      (window.innerHeight || document.documentElement.clientHeight) &&
    bounding.right <=
      (window.innerWidth || document.documentElement.clientWidth)
  );
};
var countEl = document.getElementById('count-moves');
var counted = false;
if (countEl) {
  window.addEventListener('scroll', function () {
    if(isInViewport(countEl) && !counted) {
      var countYear = new CountUp('count-moves', 2000);
      countYear.start();
      var countInternational = new CountUp('count-intern', 100);
      countInternational.start();
      var countDays = new CountUp('count-days', 7);
      countDays.start();
      
      counted = true;
    }
  }, false);
}
// canvas curves
/*var startingX = 190,
  startingY = 300,
  firstX = 350,
  firstY = 498,
  secondX = 239,
  secondY = 481,
  endingX = 690,
  endingY = 300;

var c = document.getElementById('curveUp');
var ctx = c.getContext('2d');
ctx.beginPath();
ctx.moveTo(190, 300);
ctx.bezierCurveTo(350, 498, 239, 481, 690, 300);
ctx.stroke();
*/

export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
